module.exports = {
  root: true,
  extends: [
    'airbnb-typescript',
    '@spryrocks/eslint-config',
    'prettier/react',
  ],
  plugins: [
    "eslint-plugin-no-inline-styles",
  ],
  rules: {
    "react/prop-types": 'off' ,
    'react/jsx-props-no-spreading': 'off',
    "import/extensions": 'off',
    "no-inline-styles/no-inline-styles": 'error',
    'react/require-default-props': 'off',
  },
};
